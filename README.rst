=============
BlueSky Theme
=============

This is a plugable theme for Django CMS.  A simple set of three and two column theme templates.
This theme features a dynamic width of viewing area that is appropriate for most all browsers including mobile ones (without zooming).

Included templates are:

* Base - A three column template (leftmost one is the menu).
* Two Column - A two column template.

Installation
=====================

First, install the app like so:

::

    pip install -e git+https://bitbucket.org/oddotterco/bluesky_theme.git#egg=bluesky_theme

Next, add 'bluesky_theme' to your INSTALLED_APPS list, like this:

::

    #!python

    INSTALLED_APPS = (
        ...
        'bluesky_theme',
        ...
    )

This theme uses/requires 'django-less' (https://pypi.python.org/pypi/django-less), so make sure that is installed and added to the INSTALLED_APPS list as well.

You will then see the templates from this theme available in your templates drop down when editing your site.

Advanced Installation
=====================


To gain some additional functionality and consistancy within your site, you should also:

* Create a base.html file in your site's templates directory that simply extends this theme, like so:

::

    {% extends "bluesky_theme/bluesky_theme.html" %}

* If you are using the TinyMCE editor, add this theme's edit.css to your editor's settings, like so:

::

    TINYMCE_DEFAULT_CONFIG = {
        ...
        'content_css' : STATIC_URL + "bluesky_theme/css/edit.css",
        ...
    }
